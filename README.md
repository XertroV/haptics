# Haptics <sup><sub>Trackmania ManiaPlanet Turbo</sub></sup>
Attach external haptic devices to vehicle parameters (RPM, speed, gear, etc.) and monitor their states.

## Disclaimer
This plugin is in *proof-of-concept* state. I am not responsible for any possible damages or unwanted actions this plugin may bring. Be careful of your surroundings and use at your own risk!

## Installation
- Download and run [Intiface Desktop](https://github.com/intiface/intiface-desktop/releases) and [Haptics Web Bridge](https://openplanet.dev/file/102).
- Turn on your devices (connect manually if needed).
- You can manually specify the address, port and key in the plugin settings *(Openplanet > Settings > Haptics > Server)*.
- Update devices in the plugin, change their settings and have fun!

Please refer to the [Installation](https://gitlab.com/fentrasLABS/openplanet/haptics/-/wikis/Installation) page for complete guide.

## Download
- Openplanet ([Plugin](https://openplanet.dev/plugin/haptics), [Bridge](https://openplanet.dev/file/102))
- Releases ([Plugin](https://gitlab.com/fentrasLABS/openplanet/haptics/-/releases), [Bridge](https://gitlab.com/fentrasLABS/openplanet/haptics-web-bridge/-/releases))

## Media
![](_git/1.png)
![](_git/2.png)

## Credits
- *Project icon* by [Kenney Game Icons](https://kenney.nl/assets/game-icons)
- *UI tabs* by [Miss Plugin Manager](https://github.com/openplanet-nl/plugin-manager/tree/master/src/Interface)