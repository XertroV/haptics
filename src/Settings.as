[Setting name="Welcome Screen" description="Show welcome screen on startup" category="General"]
bool Setting_ShowWelcome = true;

[Setting name="Port" description="Port to access the bridge application" category="Server"]
int Setting_ServerPort = 8008;

[Setting name="Address" description="Address (IP or host name) to access the bridge application" category="Server"]
string Setting_ServerAddress = "localhost";

[Setting name="Key" description="Key to access the bridge application" category="Server"]
string Setting_ServerKey = "gamebox";