class Game
{
    bool initialised = false;

    CTrackMania@ app;
    CSceneVehicleVisState@ visState;

    Game()
    {
        @app = cast<CTrackMania>(GetApp());
    }

    void Initialise(bool state = true)
    {
        if (state) {
            if (app.GameScene !is null && (app.CurrentPlayground !is null || app.Editor !is null)) {
                initialised = true;
            }   
        } else {
            initialised = false;
            API::ResetDevices();
            @visState = null;
        }
    }

    void Update(float dt)
    {
        if (initialised) {
            @visState = VehicleState::ViewingPlayerState();
        }
        for (uint i = 0; i < API::Devices.Length; i++) {
            API::Device@ device = API::Devices[i];
            if (device.action != API::Action::None && visState !is null) {
                switch (device.action) {
                    case API::Action::RPM:      device.power.main = int((VehicleState::GetRPM(visState) * 100) / 11000); break;
                    case API::Action::Speed:    device.power.main = int((visState.FrontSpeed * 100) / 270); break;
                    case API::Action::Gear:     device.power.main = int((visState.CurGear * 100) / 6); break;
                    case API::Action::Steering: device.power.main = int((visState.InputSteer * 100) / (visState.InputSteer > 0 ? 1.f : -1.f)); break;
                }
            } else if (device.action != API::Action::None && visState is null) {
                device.power.main = 0;
            }
            // this is literally sends a signal every frame
            // to properly check it I need to grab the received value
            // which seems to not be supported in the bridge
            device.Vibrate();
        }
    }
}