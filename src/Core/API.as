// This API is a very temporary solution
// It is not a representation of my coding skills :)

namespace API
{
    string get_URL() { return "http://" + Setting_ServerAddress + ":" + Setting_ServerPort + "/api/Device/"; }
    
    Response@ response;
    Net::HttpRequest@ http;
    array<Device@> Devices;

    void UpdateDevices()
    {
        if (response is null || !response.running) {
            @response = API::List();
        }
    }

    void ResetDevices()
    {
        for (uint i = 0; i < Devices.Length; i++) {
            Devices[i].Reset();
        }
    }

    void RemoveDevices()
    {
        ResetDevices();
        Devices.RemoveRange(0, Devices.Length);
    }

    void CheckResponse(Response@ resp)
    {
        if (resp !is null && resp.running && resp.status == Status::Done) {
            if (resp.action == "list") {
                RemoveDevices();
                for (uint i = 0; i < resp.body.Length; i++) {
                    Devices.InsertLast(API::Device(resp.body[i]));
                }
                window.UpdateTabs();
            }
            resp.running = false;
        }
    }

    void Update(float dt)
    {
        for (uint i = 0; i < Devices.Length; i++) {
            CheckResponse(Devices[i].responseLocal);
        }
        CheckResponse(response);
    }

    Response@ List()
    {
        if (http is null) {
            @http = Net::HttpRequest();
        } else {
            return Response(API::Status::Busy);
        }

        http.Method = Net::HttpMethod::Get;
        http.Headers.Set("SecretKey", Setting_ServerKey);
        http.Url = URL + "List";

        return Response("list");
    }

    Response@ Vibrate(Device@ device)
    {
        if (device.httpLocal is null) {
            @device.httpLocal = Net::HttpRequest();
        } else {
            return Response(API::Status::Busy);
        }

        device.httpLocal.Method = Net::HttpMethod::Get;
        device.httpLocal.Headers.Set("SecretKey", Setting_ServerKey);
        device.httpLocal.Url = URL + "VibrateCmd/" + Net::UrlEncode(device.name) + "/" + device.power.main;

        return Response("vibrate", @device);
    }

    class DevicePower
    {
        int main;
        DevicePower() { main = 0; }
    }

    class Device
    {
        string name;
        Action action;
        DevicePower@ power;
        Net::HttpRequest@ httpLocal;
        Response@ responseLocal;

        Device(string title)
        {
            name = title;
            Reset(true);
        }

        void Vibrate()
        {
            @responseLocal = API::Vibrate(this);
        }

        void Reset(bool withAction = false)
        {
            @power = DevicePower();
            if (withAction) {
                action = Action::None;
            }
        }
    }

    class Response
    {
        string action;
        bool running;
        API::Status status;
        string[] body;

        Response(string _action)
        {
            action = _action;
            running = true;
            status = API::Status::Empty;
            startnew(CoroutineFunc(ProcessData));            
        }

        Response(string _action, Device@ device)
        {
            action = _action;
            running = true;
            status = API::Status::Empty;
            startnew(CoroutineFuncUserdata(ProcessData), device);
        }

        Response(API::Status response) { status = response; }

        private void ProcessData()
        {
            http.Start();

            while (!http.Finished()) yield();

            Json::Value json = Json::Parse(http.String());

            if (json.GetType() != Json::Type::Null) {
                Json::Value devices = json["Devices"];
                for (uint i = 0; i < devices.Length; i++) {
                    body.InsertLast(devices[i]);
                }
            }

            status = API::Status::Done;
            @http = null;
        }

        private void ProcessData(ref@ deviceRef)
        {
            Device@ device = cast<Device>(deviceRef);

            device.httpLocal.Start();

            while (!device.httpLocal.Finished()) yield();

            Json::Value json = Json::Parse(device.httpLocal.String());

            status = API::Status::Done;
            @device.httpLocal = null;
        }
    }

    enum Status
    {
        Empty,
        Busy,
        Fail,
        Done
    }

    enum Action
    {
        None,
        RPM,
        Speed,
        Gear,
        Steering
        // Turbo
    }
}