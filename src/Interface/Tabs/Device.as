class DeviceTab : Tab {
    private string title;
    private API::Device@ device;

    DeviceTab(API::Device@ deviceRef)
    {
        @device = deviceRef;
        title = device.name;
    }

    string GetLabel() override { return title; }

    void Render() override
    {
        if (device.action != API::Action::None) UI::BeginDisabled();
        device.power.main = UI::SliderInt("Amount", device.power.main, 0, 100);
        if (device.action != API::Action::None) UI::EndDisabled();

		if (UI::BeginCombo("Action", tostring(device.action).Replace("_", " "))) {
			for (int i = 0; i < 6; i++) {
				if (tostring(API::Action(i)) == tostring(i)) continue; // thanks to NaNInf
				if (UI::Selectable(tostring(API::Action(i)).Replace("_", " "), false)) {
					device.action = API::Action(i);
				}
			}
			UI::EndCombo();
		}

        if (UI::Button("Reset to Default")) {
            device.Reset(true);
        }
    }
}