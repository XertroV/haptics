class GeneralTab : Tab {
    string GetLabel() override { return "General"; }

    void Render() override
    {
        vec2 button_size = UI::CalcItemSize(vec2(150, 0), 0.0f, 0.0f);
        vec2 avail = UI::GetWindowSize();

        vec2 centre_position_for_button = vec2(
            (avail.x - button_size.x) / 2,
            ((avail.y - button_size.y) / 2) - 16
        );

        UI::SetCursorPos(centre_position_for_button);

        if (UI::Button("Update Devices", button_size)) {
            API::UpdateDevices();
        }
    }
}